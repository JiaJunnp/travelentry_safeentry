﻿//============================================================
// Student Number : S10205579, S10194248
// Student Name : Liew Jia Jun, Nixon Lee
// Module Group : P06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace TEST_ASG2
{
    class Visitor : Person
    {

        //Attributes
        public string PassportNo { get; set; }
        public string Nationality { get; set; }

        //Constructors
        public Visitor(string n, string ppn, string ntly) : base(n)
        {
            Name = n;
            PassportNo = ppn;
            Nationality = ntly;
        }

        //Methods
        public override double CalculateSHNCharges()
        {

            return (200 + (200 * 0.07));


        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
