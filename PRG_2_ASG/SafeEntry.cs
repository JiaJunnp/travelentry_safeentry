﻿//============================================================
// Student Number : S10205579, S10194248
// Student Name : Liew Jia Jun, Nixon Lee
// Module Group : P06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace TEST_ASG2
{
    //SafeEntryClass
    class SafeEntry
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public BusinessLocation location { get; set; }

        public SafeEntry(DateTime checkin) { }

        public SafeEntry(DateTime c, BusinessLocation l)
        {
            CheckIn = c;
            location = l;
        }

        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
            location.VisitorsNow -= 1;
        }

        public override string ToString()
        {
            //checkin date, check out date , bo need to include location
            return base.ToString();
        }

    }
}
