﻿//============================================================
// Student Number : S10205579, S10194248
// Student Name : Liew Jia Jun, Nixon Lee
// Module Group : P06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace TEST_ASG2
{
    class TraceTogetherToken
    {

        //Attributes
        public string SerialNo { get; set; }
        public string CollectionLocation { get; set; }
        public DateTime ExpiryDate { get; set; }


        //Constructor
        public TraceTogetherToken() { }
        public TraceTogetherToken(string sn, string cl, DateTime ed)
        {
            SerialNo = sn;
            CollectionLocation = cl;
            ExpiryDate = ed;
        }

        //Method
        public bool IsEligibleForReplacement()
        {
            DateTime Today = DateTime.Today;

            int daysleft = (Today.Date - ExpiryDate.Date).Days;

            if (daysleft >= 0 && daysleft <= 30)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}

