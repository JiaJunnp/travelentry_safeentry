﻿//============================================================
// Student Number : S10205579, S10194248
// Student Name : Liew Jia Jun, Nixon Lee
// Module Group : P06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace TEST_ASG2
{
    class SHNFacility
    {
        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double DistFromAirCheckpoint { get; set; }
        public double DistFromSeaCheckpoint { get; set; }
        public double DistFromLandCheckpoint { get; set; }
        public SHNFacility() { }
        public SHNFacility(string fn, int fc, double dac, double dsc, double dlc)
        {
            FacilityName = fn;
            FacilityCapacity = fc;
            DistFromAirCheckpoint = dac;
            DistFromSeaCheckpoint = dsc;
            DistFromLandCheckpoint = dlc;
        }
       
        public double CalculateTravelCost(string em, DateTime en)
        {
            string h = en.ToString("HH");
            double hour = Convert.ToDouble(h);


            if (FacilityName.ToUpper() == "A'RESORT" || FacilityName.ToUpper() == "A RESORT")
            {
                if (em.ToUpper() == "AIR")
                {
                    double air = 5.1 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + air;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 +air;
                        double total = fare +fare*25/100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + air;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }
                else if (em.ToUpper() == "SEA")
                {
                    double sea = 14.2 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + sea;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 +sea;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + sea;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }
                else
                {
                    double land = 26.2 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + land;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + land;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + land;
                        double total = (fare + (fare) * 50 / 100);
                        return total; 
                    }
                }


            }

            else if (FacilityName.ToUpper() == "YOZEL")
            {
                if (em.ToUpper() == "AIR")
                {
                    double air = 16.2 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + air;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + air;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + air;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }
                else if (em.ToUpper() == "SEA")
                {
                    double sea = 4.2 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + sea;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + sea;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + sea;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }
                else
                {
                    double land = 20.4 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + land;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + land;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + land;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }



            }
            else if (FacilityName.ToUpper() == "MANDARIN ORCHARD")
            {
                if (em.ToUpper() == "AIR")
                {
                    double air = 12.8 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + air;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + air;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + air;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }
                else if (em.ToUpper() == "SEA")
                {
                    double sea = 6 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + sea;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + sea;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + sea;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }
                else
                {
                    double land = 16 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + land;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + land;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + land;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }

            }
            else
            {
                if (em.ToUpper() == "AIR")
                {
                    double air = 2.2 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + air;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + air;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + air;
                        double total = (fare + (fare) * 50 / 100);

                        return total;
                    }
                }
                else if (em.ToUpper() == "SEA")
                {
                    double sea = 16.9 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + sea;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour>=18 && hour<24))
                    {
                        double fare = 50 + sea;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + sea;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }
                else
                {
                    double land = 18.4 * 0.22;
                    if (hour >= 9 && hour < 18)
                    {
                        double fare = 50 + 0;
                        return fare + land;
                    }
                    else if ((hour >= 6 && hour < 9) || (hour >= 18 && hour < 24))
                    {
                        double fare = 50 + land;
                        double total = fare + fare * 25 / 100;
                        return total;
                    }
                    else
                    {
                        double fare = 50 + land;
                        double total = (fare + (fare) * 50 / 100);
                        return total;
                    }
                }

            }
        }
        public bool IsAvailable()
        {


            if (FacilityVacancy == FacilityCapacity)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
