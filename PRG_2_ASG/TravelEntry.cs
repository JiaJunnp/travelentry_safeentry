﻿//============================================================
// Student Number : S10205579, S10194248
// Student Name : Liew Jia Jun, Nixon Lee
// Module Group : P06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace TEST_ASG2
{
    class TravelEntry
    {
        //Attributes
        public string LastCountryOfEmbarkation { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ShnEndDate { get; set; }
        public SHNFacility ShnStay { get; set; }
        public bool isPaid { get; set; }
        public TravelEntry() { }

        public TravelEntry(string lcoe, string em, DateTime entd)
        {
            LastCountryOfEmbarkation = lcoe;
            EntryMode = em;
            EntryDate = entd;
            ShnStay = new SHNFacility();
        }
        public void AssignSHNFacility(SHNFacility shnf)
        {
            shnf.FacilityVacancy += 1;
            ShnStay = shnf;
        }


        public void CalculateSHNDuration()
        {
            if (LastCountryOfEmbarkation.ToUpper() == "NEW ZEALAND" || LastCountryOfEmbarkation.ToUpper() == "VIETNAM")
            {
                ShnEndDate = EntryDate.AddDays(0);

            }
            else if (LastCountryOfEmbarkation.ToUpper() == "MACAO SAR")
            {
                ShnEndDate = EntryDate.AddDays(7);
            }
            else
            {
                ShnEndDate = EntryDate.AddDays(14);

            }

        }
        public override string ToString()
        {
            return base.ToString();
        }

    }
}
