﻿//============================================================
// Student Number : S10205579, S10194248
// Student Name : Liew Jia Jun, Nixon Lee
// Module Group : P06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace TEST_ASG2
{
    //Resident Class
    class Resident : Person
    {
        //Attributes
        public string Address { get; set; }
        public DateTime LastLeftCountry { get; set; }
        public TraceTogetherToken token { get; set; }

        //Constructors
        public Resident(string n, string a, DateTime llc, TraceTogetherToken t) : base(n)
        {
            Name = n;
            Address = a;
            LastLeftCountry = llc;
            token = t;
        }


        //Methods
        public override double CalculateSHNCharges()
        {

            return (200 + (200 * 0.07));

        }

        public override string ToString()
        {
            return base.ToString();
        }

    }

}
