﻿//============================================================
// Student Number : S10205579, S10194248
// Student Name : Liew Jia Jun, Nixon Lee
// Module Group : P06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace TEST_ASG2
{
    //Business Location Class
    class BusinessLocation
    {
        public string BusinessName { get; set; }
        public string BranchCode { get; set; }
        public int MaximumCapacity { get; set; }
        public int VisitorsNow { get; set; }

        public BusinessLocation() { }

        public BusinessLocation(string bn, string bc, int m)
        {
            BusinessName = bn;
            BranchCode = bc;
            MaximumCapacity = m;
        }

        public bool IsFull()
        {
            if (VisitorsNow >= MaximumCapacity)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public override string ToString()
        {
            return base.ToString();
        }
    }
}
