﻿//============================================================
// Student Number : S10205579, S10194248
// Student Name : Liew Jia Jun, Nixon Lee
// Module Group : P06
//============================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Globalization;

namespace TEST_ASG2
{
    class Program
    {

        //Main method
        static void Main(string[] args)
        {
            //Create Person and Business Location List 
            List<Person> personList = new List<Person>();
            List<BusinessLocation> blist = new List<BusinessLocation>();
            List<SHNFacility> SHNFlist = new List<SHNFacility>();
            
            InitSHNFdata(SHNFlist);
            InitPersonList(personList);
            InitBusinessList(blist);
            




            while (true)
            {
                DisplayMenu();
                Console.Write("Enter Option(or 0 to exit): ");
                string option = Console.ReadLine();
                if (option == "1")
                {
                    Console.WriteLine("Person and Business Data Loaded.");
                    Console.WriteLine("");
                }
                else if (option == "2")
                {
                    
                    Console.WriteLine("SHN Facility Data Loaded.");
                    Console.WriteLine("");
                }
                else if (option == "3")
                {
                    ListVisitors(personList);
                }
                else if (option == "4")
                {
                    ListPersonDetails(personList);
                }
                else if (option == "5")
                {
                    AssignReplaceTTToken(personList);
                }
                else if (option == "6")
                {
                    ListBizLocations(blist);
                }
                else if (option == "7")
                {
                    EditBizLocations(blist);
                }
                else if (option == "8")
                {
                    SafeEntryCheckin(personList, blist);
                }
                else if (option == "9")
                {
                    SECheckout(personList,  blist);
                }
                else if (option == "10")
                {
                    DisplaySHNFacility(SHNFlist);

                }
                else if (option == "11")
                {
                    CreateVisitor(personList);
                }
                else if (option == "12")
                {
                    CreateTravelEntryRecord(personList, SHNFlist);
                }
                else if (option == "13")
                {
                    CalculateSHNCharges(personList);
                }
                else if(option=="14")
                {
                    ContactTracingReporting(personList, blist);
                }
                else if(option=="15")
                {
                    SHNStatusReporting(personList);
                }
                else if(option=="16")
                {
                    CovidChecking();
                }
                else if (option == "0")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Option! Please try again..");
                    Console.WriteLine();
                }

            }



        }


        //DisplayMenuMethod
        static void DisplayMenu()
        {
            Console.WriteLine("1) Load Person and Business Location Data");
            Console.WriteLine("2) Load SHN Facility Data");
            Console.WriteLine("3) List all Visitors");
            Console.WriteLine("4) List Person Details");
            Console.WriteLine("5) Assign/Replace TraceTogether Token");
            Console.WriteLine("6) List all Business Locations");
            Console.WriteLine("7) Edit Business Location Capacity");
            Console.WriteLine("8) SafeEntry Check-in");
            Console.WriteLine("9) SafeEntry Check-out");
            Console.WriteLine("10) List all SHN Facilities");
            Console.WriteLine("11) Create Visitor");
            Console.WriteLine("12) Create TravelEntry Record");
            Console.WriteLine("13) Calculate SHN Charges");
            Console.WriteLine("14) Contact Tracing Reporting");
            Console.WriteLine("15) SHN Status Reporting");
            Console.WriteLine("16) Covid-19 Declaration form");

        }

        //****BASIC FEATURES****

        //===GENERAL FEATURES==
        // 1) Load Person and Business Location Data Methods
        static void InitPersonList(List<Person> personList)
        {

            using (StreamReader sr = new StreamReader("Person.csv"))
            {
                string s = sr.ReadLine();
                while ((s = sr.ReadLine()) != null)
                {
                    string[] items = s.Split(',');
                    if (items[0].ToUpper() == "RESIDENT")
                    {
                        if (items[6] != "")
                        {
                            DateTime time = Convert.ToDateTime(items[8]);
                            string[] tim = items[8].Split("/");
                            DateTime expdate = new DateTime(Convert.ToInt32(tim[2]), Convert.ToInt32(tim[1]), Convert.ToInt32(tim[0]));

                            TraceTogetherToken token = new TraceTogetherToken(items[6], items[7], Convert.ToDateTime(items[8]));

                            Person r1 = new Resident(items[1], items[2], Convert.ToDateTime(items[3]), token);

                            if (items[9] != "")
                            {
                                TravelEntry te = new TravelEntry(items[9], items[10], Convert.ToDateTime(items[11]));

                                te.ShnEndDate = Convert.ToDateTime(items[12]);
                                if (items[14].ToUpper() != "")
                                {
                                    te.ShnStay.FacilityName = items[14];
                                }
                                else
                                {
                                    te.ShnStay.FacilityName = null;
                                }
                                if (items[13].ToUpper() == "TRUE")
                                {
                                    te.isPaid = true;
                                }
                                else if (items[13].ToUpper() == "FALSE")
                                {
                                    te.isPaid = false;
                                }

                                r1.AddTravelEntry(te);


                            }
                            personList.Add(r1);
                        }

                        else
                        {
                            TraceTogetherToken token = new TraceTogetherToken(" ", " ", default(DateTime));

                            Person r1 = new Resident(items[1], items[2], Convert.ToDateTime(items[3]), token);
                           

                            if (items[9] != "")
                            {
                                TravelEntry te = new TravelEntry(items[9], items[10], Convert.ToDateTime(items[11]));

                                te.ShnEndDate = Convert.ToDateTime(items[12]);
                                if (items[14].ToUpper() != "")
                                {
                                    te.ShnStay.FacilityName = items[14];
                                }
                                else
                                {
                                    te.ShnStay.FacilityName = null;
                                }
                                if (items[13].ToUpper() == "TRUE")
                                {
                                    te.isPaid = true;
                                }
                                else if (items[13].ToUpper() == "FALSE")
                                {
                                    te.isPaid = false;
                                }

                                r1.AddTravelEntry(te);


                            }
                            personList.Add(r1);
                        }


                        

                    }
                    else if (items[0].ToUpper() == "VISITOR")
                    {
                        Person v1 = new Visitor(items[1], items[4], items[5]);
                        if (items[9] != "")
                        {
                            TravelEntry te = new TravelEntry(items[9], items[10], Convert.ToDateTime(items[11]));

                            te.ShnEndDate = Convert.ToDateTime(items[12]);

                            if (items[14].ToUpper() != "")
                            {
                                te.ShnStay.FacilityName = items[14].ToUpper();
                            }
                            else
                            {
                                te.ShnStay.FacilityName = null;
                            }
                            if (items[13].ToUpper() == "TRUE")
                            {
                                te.isPaid = true;
                            }
                            else if (items[13].ToUpper() == "FALSE")
                            {
                                te.isPaid = false;
                            }

                            v1.AddTravelEntry(te);
                        }
                        personList.Add(v1);
                    }

                }

            }

            
        }




        static void InitBusinessList(List<BusinessLocation> blist)
        {
            string[] bdetails = File.ReadAllLines("BusinessLocation.csv");
            for (int i = 1; i < bdetails.Length; i++)
            {
                string[] bdata = bdetails[i].Split(",");
                string bname = bdata[0];
                string bcode = bdata[1];
                int maxcap = Convert.ToInt32(bdata[2]);
                BusinessLocation business = new BusinessLocation(bname, bcode, maxcap);
                blist.Add(business);
            }
        }







        // 2) Load SHN Facility Data  (API)
        static void InitSHNFdata(List<SHNFacility> SHNFlist)
        {
            List<SHNFacility> SHNFList = new List<SHNFacility>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    SHNFList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);


                    foreach (SHNFacility sf in SHNFList)
                    {
                        SHNFacility sf1 = new SHNFacility(sf.FacilityName, sf.FacilityCapacity, sf.DistFromAirCheckpoint, sf.DistFromSeaCheckpoint, sf.DistFromLandCheckpoint);
                        SHNFlist.Add(sf1);




                    }


                }
            }
            
        }



        // 3) List all Visitors
        static void ListVisitors(List<Person> personList)
        {
            Console.WriteLine("{0, -15} {1, -15} {2, -15}", "Name", "PassportNo", "Nationality");
            foreach (var i in personList)
            {
                if (i is Visitor)
                {
                    Visitor v = (Visitor)i;
                    Console.WriteLine("{0, -15} {1, -15} {2, -15}", v.Name, v.PassportNo, v.Nationality);
                }

            }
        }


        //===SafeEntry/TraceTogether===

        // 4) List Person Details
        static void ListPersonDetails(List<Person> personList)
        {
            double loop = 1;
            double a = 0;
            while (loop == 1)

            {
                Console.Write("Enter your name ");
                double count = 0;
                string name = Console.ReadLine();
                foreach (Person p in personList)
                {
                    count += 1;
                    if (p.Name.ToUpper() == name.ToUpper())
                    {
                        loop = 0;

                        if (p is Visitor)
                        {
                            Visitor v = (Visitor)p;
                            foreach (TravelEntry te in v.TravelEntryList)
                            {
                                Console.WriteLine("{0,-27} {1,-12} {2,-23} {3,-23} {4,-9} {5}", "LastCountryOfEmbarkation", "EntryMode", "EntryDate", "ShnDate", "IsPaid", "Facility Name");
                                Console.WriteLine("{0,-27} {1,-12} {2,-23} {3,-23} {4,-9} {5}", te.LastCountryOfEmbarkation, te.EntryMode, te.EntryDate, te.ShnEndDate, te.isPaid, te.ShnStay.FacilityName);
                                Console.WriteLine();
                                a = 1;

                            }
                            if (a == 0)
                            {
                                Console.WriteLine("No Travel Entry Found!");
                                Console.WriteLine();
                            }
                        }
                        else if (p is Resident)
                        {

                            Resident r = (Resident)p;

                            if (r.token.SerialNo!=" ")
                            { 
                                Console.WriteLine("{0,-8} {1,-20} {2,-21} {3,-18} {4,-13} {5,10}", "Name", "Address", "lastLeftCountry", "tokenSerial", "tokenCollectionLocation", "tokenExpiryDate");
                                Console.WriteLine("{0,-8} {1,-20} {2,-21} {3,-18} {4,-13} {5,20}", p.Name, r.Address, r.LastLeftCountry, r.token.SerialNo, r.token.CollectionLocation, r.token.ExpiryDate.ToShortDateString());
                            }
                            else
                            {
                                Console.WriteLine("{0,-8} {1,-20} {2,-21}", "Name", "Address", "lastLeftCountry");
                                Console.WriteLine("{0,-8} {1,-20} {2,-21}", p.Name, r.Address, r.LastLeftCountry);
                            }


                        }

                    }
                }
                if (personList.Count == count && loop == 1)
                {
                    Console.WriteLine("Person not found!");
                    Console.WriteLine();
                }
            }








        }


        // 5) Assign/Replace TraceTogether Token (DONE)
        static void AssignReplaceTTToken(List<Person> personList)
        {
            Console.Write("Enter the resident name: "); //prompt for resident name
            string targetname = Console.ReadLine();     // recieve input


            bool nfound = false;

            foreach (Person p in personList)
            {
                if (targetname.ToUpper() == p.Name.ToUpper())
                {
                    nfound = true;
                    if (p is Resident)
                    {
                        Resident r = (Resident)p;

                        if (r.token.SerialNo == " ")  // if the resident does not have a tracetogether token
                        {

                            Console.WriteLine("No tracetogether token found. Assigning one now");

                            Random rng = new Random();//serial no
                            string num = rng.Next(0, 100000).ToString("D5");
                            string serialno = "T" + num;

                            Console.Write("Enter Collection location: "); //user input collection lcoation
                            string collectlocation = Console.ReadLine();

                            DateTime newexpiredate = DateTime.Today.AddMonths(6);//new expire date

                            TraceTogetherToken newtoken = new TraceTogetherToken(serialno, collectlocation, newexpiredate);
                            r.token = newtoken;
                            Console.WriteLine("New token assigned");
                            Console.WriteLine("Serial No: {0} \tCollection Location: {1} \tExpiry Date: {2}", r.token.SerialNo, r.token.CollectionLocation, r.token.ExpiryDate.ToShortDateString());
                            Console.WriteLine();
                            break;

                        }


                        else if (r.token.SerialNo != " ")
                        {

                            //If resident has token but has already expiresd
                            if (r.token.IsEligibleForReplacement())
                            {
                                Console.WriteLine("Trace together token has already expired. Assigning a new one now");

                                Random rng = new Random();//serial no
                                string num = rng.Next(0, 100000).ToString("D5");
                                string serialno = "T" + num;

                                Console.Write("Enter Collection location: "); //user input collection lcoation
                                string collectlocation = Console.ReadLine();

                                DateTime newexpiredate = DateTime.Today.AddMonths(6);//new expire date

                                TraceTogetherToken newtoken = new TraceTogetherToken(serialno, collectlocation, newexpiredate);

                                r.token = newtoken;
                                Console.WriteLine("Token has been replaced");
                                Console.WriteLine("Serial No: {0} \tCollection Location: {1} \tExpiry Date: {2}", r.token.SerialNo, r.token.CollectionLocation, r.token.ExpiryDate.ToShortDateString());
                                Console.WriteLine();
                                break;
                            }


                            //expiry date passed
                            else if (!r.token.IsEligibleForReplacement())
                            {
                                Console.WriteLine("Token cannot be replaced");
                                Console.WriteLine();
                                break;
                            }

                        }


                    }
                }

            }
            if (!nfound)
            {
                Console.WriteLine();
                Console.WriteLine("Invalid name entered! ");
            }

        }

        // 6) List all Business Locations (DONE)
        static void ListBizLocations(List<BusinessLocation> blist)
        {
            Console.WriteLine("{0,-20} {1,10} {2,20}", "Business Name", "Branch Code", "Maximum Capacity");

            foreach (var b in blist)
            {
                Console.WriteLine("{0,-20} {1,10} {2,20}", b.BusinessName, b.BranchCode, b.MaximumCapacity);

            }
            Console.WriteLine();
        }




        // 7) Edit Business Location Capacity (DONE)
        static void EditBizLocations(List<BusinessLocation> blist)
        {
            Console.Write("Enter the Business Name to be edited: ");
            string targetbizname = Console.ReadLine();

            bool found = false;
            foreach (var b in blist)
            {

                if (targetbizname.ToUpper() == b.BusinessName.ToUpper())
                {
                    found = true;
                    Console.Write("Enter new maximum capacity: ");

                    try { 
                    int newcap = Convert.ToInt32(Console.ReadLine());
                    b.MaximumCapacity = newcap;
                    Console.WriteLine("Maximum Capacity of {0} has been updated", targetbizname);
                    Console.WriteLine();
                    }
                    catch
                    {
                        Console.WriteLine("Please enter a valid number!");
                        Console.WriteLine();
                    }
                }

            }
            if (!found)
            {
                Console.WriteLine("Invalid Name Entered! Please try again");
                EditBizLocations(blist);
            }


        }


        // 8) SafeEntry Check-in  (DONE)
        static void SafeEntryCheckin(List<Person> personlist, List<BusinessLocation> blist)
        {
            Console.WriteLine();
            Console.WriteLine("===Safe Entry CheckIn===");
            Console.Write("Enter your name: "); //prompt user
            string name = Console.ReadLine(); //get user name

            bool nfound = false;
            bool bfound = false;
            bool nocheckout = false;

            foreach (var p in personlist)
            {

                if (name.ToUpper() == p.Name.ToUpper()) //name validation
                {
                    nfound = true;
                    ListBizLocations(blist);//list location

                    Console.Write("Enter the name of the business location you are checking in: "); // prompt business location
                    string tbizname = Console.ReadLine(); // get name of location


                    foreach (var b in blist)  // check entire list
                    {
                        if (tbizname.ToUpper() == b.BusinessName.ToUpper()) // check if target business name exists
                        {

                            bfound = true;


                            if (b.IsFull())   //If location is full
                            {
                                Console.WriteLine("{0} is full! Maximum capacity has already been reached!", tbizname);
                                Console.WriteLine();

                            }


                            else if (!b.IsFull()) //if location is not full 
                            {

                                foreach (var serecords in p.SafeEntryList) //check in any check in details in list
                                {
                                    if (serecords.CheckOut == DateTime.MinValue && serecords.location.BusinessName == b.BusinessName)
                                    {
                                        Console.WriteLine("You have already checked-in to this location");
                                        Console.WriteLine();
                                        nocheckout = true;
                                        break;
                                    }

                                }

                                if (!nocheckout) //if no checkin
                                {
                                    b.VisitorsNow += 1; // increase visitor now by 1
                                    DateTime Checkin = DateTime.Now;
                                    BusinessLocation newlocation = new BusinessLocation(b.BusinessName, b.BranchCode, b.MaximumCapacity);
                                    SafeEntry se = new SafeEntry(Checkin, newlocation);
                                    p.AddSafeEntry(se);// add the safeentry object to p
                                    Console.WriteLine("Checked-in to {0}", b.BusinessName);
                                    Console.WriteLine();
                                    break;
                                }
                            }

                        }

                    }
                    if (!bfound) //business name not found
                    {
                        Console.WriteLine("Invalid Business Name Entered! Please try again. ");
                        Console.WriteLine();
                        break;
                    }

                }

            }
            if (!nfound) //person name not found
            {
                Console.WriteLine("Invalid Name Entered! Please try again ");
                Console.WriteLine();

            }
        }



        // 9) SafeEntry Check-out (Done)
        static void SECheckout(List<Person> personlist, List<BusinessLocation> blist)
        {
            Console.WriteLine();
            Console.WriteLine("===Safe Entry CheckOut===");
            Console.Write("Enter your name: "); //prompt user
            string name = Console.ReadLine(); //get user name

            bool nfound = false;
            foreach (var p in personlist)
            {
                if (name.ToUpper() == p.Name.ToUpper())
                {
                    nfound = true;

                    bool yesrecord = false;


                    Console.WriteLine();

                    int count = 1;


                    DateTime yes = new DateTime(1 / 1 / 2020);
                    foreach (var serecords in p.SafeEntryList)// display the se list
                    {


                        if (serecords.CheckOut == default)
                        {
                            Console.WriteLine("  {0,-7}  {1,-20} {2,-21} {3,-21} {4,-21}", "Name", "Check-in Time", "Check-out Time", "Business Name", "Branch Code");// header
                            yesrecord = true;

                            Console.WriteLine(count + ") {0,-7} {1,-20}  {2,-21} {3,-21} {4,-21}", p.Name, serecords.CheckIn, " ", serecords.location.BusinessName, serecords.location.BranchCode);
                            count += 1;

                            Console.WriteLine();

                            try
                            {

                                foreach(var b in blist)
                                {
                                  
                                    if (serecords.location.BusinessName == b.BusinessName)
                                    {

                                        Console.Write("Enter the number of the record to be checked out: "); //prompt user for entry num
                                        int choice = Convert.ToInt32(Console.ReadLine()); //get user input
                                        b.VisitorsNow --;
                                        serecords.PerformCheckOut(); // CALL perform checkout fucntion
                                        Console.WriteLine("Checked out of {0} at {1}", serecords.location.BusinessName, serecords.CheckOut);
                                         Console.WriteLine();
                                    break;
                                    }
                                    //p.SafeEntryList.RemoveAt(choice - 1);
                                }
                                
                            }



                            catch (Exception e)
                            {
                                Console.WriteLine("Please enter a valid Number! ");
                                Console.WriteLine(e.Message);
                            }

                        }

                        else
                        {
                            Console.WriteLine("  {0,-7}  {1,-20} {2,-21} {3,-21} {4,-21}", "Name", "Check-in Time", "Check-out Time", "Business Name", "Branch Code");// header
                            yesrecord = true;

                            Console.WriteLine(count + ") {0,-7} {1,-20}  {2,-21} {3,-21} {4,-21}", p.Name, serecords.CheckIn, serecords.CheckOut, serecords.location.BusinessName, serecords.location.BranchCode);
                            count += 1;

                            Console.WriteLine("You have already checked out");
                            Console.WriteLine();

                        }


                    }
                    if (!yesrecord)
                    {
                        Console.WriteLine("You dont have an existing check in record.");
                        Console.WriteLine();
                        break;
                    }


                }
            }

            if (!nfound)
            {
                Console.WriteLine("Invalid Name entered! Please try again!");
                Console.WriteLine();
            }
        }


        //===TravelEntry===
        // 10) List all SHN Facilities
        static void DisplaySHNFacility(List<SHNFacility> SHNFlist)
        {
            Console.WriteLine("{0,-16} {1} {2} {3} {4}", "Facility Name", "Capacity", "DistFromAirCheckpoint", "DistFromSeaCheckpoint", "DistFromLandCheckpoint");
            foreach (SHNFacility sf in SHNFlist)
            {
                Console.WriteLine("{0,-16} {1,-8} {2,-21} {3,-21} {4}", sf.FacilityName, sf.FacilityCapacity, sf.DistFromAirCheckpoint, sf.DistFromSeaCheckpoint, sf.DistFromLandCheckpoint);

            }
        }

        // 11) Create Visitor
        static void CreateVisitor(List<Person> personList)
        {
            double loop = 0;
            while (loop == 0)
            {
                double count = 0;
                Console.Write("Please enter your Name: ");
                string name = Console.ReadLine();
                foreach (Person p in personList)
                {
                    count += 1;

                    if (p.Name.ToUpper() != name.ToUpper())
                    {
                        if (personList.Count == count)
                        {
                            Console.Write("Please enter your Passport Number: ");

                            string no = Console.ReadLine();

                            Console.Write("Please enter your Nationality: ");
                            string nationality = Console.ReadLine();
                            Person v1 = new Visitor(name, no, nationality);
                            personList.Add(v1);
                            Console.WriteLine(v1.Name + "'s profile successfully created!");
                            Console.WriteLine();
                            loop = 1;
                            break;



                        }


                    }
                    else if (p.Name.ToUpper() == name.ToUpper())
                    {
                        Console.WriteLine("Name already exists! Please try again.");
                        break;
                    }

                }
            }


        }

        // 12) Create TravelEntry Record
        static void CreateTravelEntryRecord(List<Person> personList, List<SHNFacility> SHNFlist)
        {
            double troo = 0;
            while (troo == 0)
            {
                Console.Write("Please enter your Name: ");
                string name = Console.ReadLine();
                double count = 0;
                foreach (Person p in personList)
                {
                    count += 1;
                    if (p.Name.ToUpper() == name.ToUpper()) // check if name is in list
                    {
                        troo = 1;


                        Console.Write("Please enter your Last Country Of Embarkation: ");
                        string country = Console.ReadLine();
                        double tru = 0;
                        while(tru==0)
                        {
                            Console.Write("Please enter your Entry Mode: ");
                            string entrymode = Console.ReadLine();
                            if (entrymode.ToUpper()=="AIR" || entrymode.ToUpper() == "SEA" || entrymode.ToUpper() == "LAND")
                            {
                                tru = 1;
                                double trew = 0;
                                while (trew==0)
                                {
                                    Console.Write("Please enter your Entry Date(YY/MM/DD) or (DD/MM/YYYY): ");
                                    try
                                    {
                                        DateTime entrydate = Convert.ToDateTime(Console.ReadLine()); // TO ADD TRY, EXCEPT to check if it is a valid datetime

                                        TravelEntry te = new TravelEntry(country, entrymode, entrydate);
                                        te.CalculateSHNDuration();
                                        DisplaySHNFacility(SHNFlist);
                                        double loop = 0;


                                        if (te.ShnStay.FacilityName is null)
                                        {
                                            if (te.ShnEndDate > te.EntryDate.AddDays(7))
                                            {
                                                while (loop == 0)
                                                {
                                                    Console.Write("Please enter your chosen SHN Facility: ");
                                                    string facility = Console.ReadLine();

                                                    foreach (SHNFacility shnf in SHNFlist)
                                                    {
                                                        if (shnf.FacilityName.ToUpper() == facility.ToUpper())
                                                        {

                                                            if (shnf.IsAvailable())
                                                            {

                                                                te.AssignSHNFacility(shnf);
                                                                Console.WriteLine("You have been assigned to Facility " + facility);
                                                                Console.WriteLine();

                                                                loop = 1;
                                                                trew = 1;
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine(shnf.FacilityName + " is currently fully occupied. Please chooose another SHN Facility");
                                                                Console.WriteLine();
                                                            }

                                                        }

                                                    }
                                                }
                                                
                                            }
                                            else
                                            {

                                                Console.WriteLine("No SHN Facility allocation required.");
                                                trew = 1;
                                            }
                                            p.AddTravelEntry(te);









                                        }
                                    }
                                    catch
                                    {
                                        Console.WriteLine("Invalid date time format entered. Please enter in the format of (DD/MM/YYYY) or (YYYY/MM/DD)");
                                        Console.WriteLine();
                                    }
                                    
                                }
                                
                            }
                            else
                            {
                                Console.WriteLine("Please enter a valid entry mode. (AIR/SEA/LAND)");
                            }
                            
                        }
                        


                    }
                    else if (p.Name.ToUpper() != name.ToUpper() && troo == 0)
                    {
                        if (personList.Count == count)
                        {
                            Console.WriteLine("Person not found! Please try again.");
                            Console.WriteLine();
                        }
                    }
                }

            }
        }

        // 13) Calculate SHN Charges    

        static void CalculateSHNCharges(List<Person> personList)
        {
            double tenonull = 0;
            double troo = 1;
            while (troo == 1)
            {
                Console.Write("Please enter your Name: ");
                string name = Console.ReadLine();
                int count = 0;
                foreach (Person p in personList)
                {

                    count += 1;
                    if (p.Name.ToUpper() == name.ToUpper())
                    {
                        troo = 0;
                        

                        if (p is Visitor)
                        {

                            Visitor v = (Visitor)p;
                           

                            foreach (TravelEntry te in p.TravelEntryList)
                            {                                
                                
                                if (te != null)
                                {
                                    
                                    if (te.ShnEndDate <= DateTime.Now)
                                    {
                                        tenonull = 1;
                                        if (te.isPaid is false) //check if they paid already
                                        {
                                            if (te.LastCountryOfEmbarkation.ToUpper() == "NEW ZEALAND" || te.LastCountryOfEmbarkation.ToUpper() == "VIETNAM" || te.LastCountryOfEmbarkation.ToUpper() == "MACAO SAR")
                                            {


                                                double shnc = v.CalculateSHNCharges();
                                                double travel = (80 + (80 * 0.07));
                                                double cost = shnc + travel;
                                                while (true)
                                                {
                                                    Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                                    Console.Write("Would you like to make payment?(Y/N) ");
                                                    string option = Console.ReadLine();
                                                    if (option.ToUpper() == "Y")
                                                    {
                                                        Console.WriteLine("Payment Done!");
                                                        Console.WriteLine();
                                                        te.isPaid = true;
                                                        break;
                                                    }
                                                    else if (option.ToUpper() == "N")
                                                    {
                                                        Console.WriteLine("Payment shall be made at another date!");
                                                        Console.WriteLine();
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                double shnc = v.CalculateSHNCharges();
                                                
                                                
                                                double travel = (te.ShnStay.CalculateTravelCost(te.EntryMode, te.EntryDate));
                                                
                                                double gst = travel * 0.07;
                                                
                                                double sdf = (2000 + (2000 * 0.07));
                                                
                                                double cost = travel + shnc + gst +sdf;
                                                while (true)
                                                {
                                                    Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                                    Console.Write("Would you like to make payment?(Y/N) ");
                                                    string option = Console.ReadLine();
                                                    if (option.ToUpper() == "Y")
                                                     {
                                                        Console.WriteLine("Payment Done!");
                                                        Console.WriteLine();
                                                        te.isPaid = true;
                                                        break;
                                                    }
                                                    else if (option.ToUpper() == "N")
                                                    {
                                                        Console.WriteLine("Payment shall be made at another date!");
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                                    }
                                                }
                                                
                                                



                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("You have already paid!");
                                            Console.WriteLine();
                                            break;
                                        }

                                    }
                                }
                                


                            }





                        }
                        else if (p is Resident)
                        {
                            Resident r = (Resident)p;
                            foreach (TravelEntry te in p.TravelEntryList)
                            {
                                if (te.EntryDate <= DateTime.Now)
                                {
                                    tenonull = 1;   
                                    if (te.isPaid is false) //check if they paid already
                                    {
                                        if (te.LastCountryOfEmbarkation.ToUpper() == "NEW ZEALAND" || te.LastCountryOfEmbarkation.ToUpper() == "VIETNAM")
                                        {


                                            double cost = r.CalculateSHNCharges();
                                            while (true)
                                            {
                                                Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                                Console.Write("Would you like to make payment?(Y/N) ");
                                                string option = Console.ReadLine();
                                                if (option.ToUpper() == "Y")
                                                {
                                                    Console.WriteLine("Payment Done!");
                                                    Console.WriteLine();
                                                    te.isPaid = true;
                                                    break;
                                                }
                                                else if (option.ToUpper() == "N")
                                                {
                                                    Console.WriteLine("Payment shall be made at another date!");
                                                    Console.WriteLine();
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                                }
                                            }

                                        }
                                        else if (te.LastCountryOfEmbarkation.ToUpper() == "MACAO SAR")
                                        {
                                            double shnc = r.CalculateSHNCharges();
                                            double travel = 20 + (20 * 0.07);
                                            double cost = travel + shnc;
                                            while (true)
                                            {
                                                Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                                Console.Write("Would you like to make payment?(Y/N) ");
                                                string option = Console.ReadLine();
                                                if (option.ToUpper() == "Y")
                                                {
                                                    Console.WriteLine("Payment Done!");
                                                    Console.WriteLine();
                                                    te.isPaid = true;
                                                    break;
                                                }
                                                else if (option.ToUpper() == "N")
                                                {
                                                    Console.WriteLine("Payment shall be made at another date!");
                                                    Console.WriteLine();
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            double shnc = r.CalculateSHNCharges();
                                            
                                            double travelSDF = 1020 + (1020 * 0.07);
                                            double cost = travelSDF + shnc;
                                            while (true)
                                            {
                                                Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                                Console.Write("Would you like to make payment?(Y/N) ");
                                                string option = Console.ReadLine();
                                                if (option.ToUpper() == "Y")
                                                {
                                                    Console.WriteLine("Payment Done!");
                                                    Console.WriteLine();
                                                    te.isPaid = true;
                                                    break;
                                                }
                                                else if (option.ToUpper() == "N")
                                                {
                                                    Console.WriteLine("Payment shall be made at another date!");
                                                    Console.WriteLine();
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                                }
                                            }
                                            



                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("You have already paid!");
                                        Console.WriteLine();
                                        break;
                                    }

                                }


                            }
                        }







                    }
                    else if (p.Name.ToUpper() != name.ToUpper() && troo == 1)
                    {
                        if (count == personList.Count)
                        {
                            Console.WriteLine("Invalid name entered! Please try again.");
                            Console.WriteLine();
                        }
                    }

                }
            }
            if (tenonull == 0)
            {
                Console.WriteLine("TravelEntry Not Found!");
                Console.WriteLine();
            }




        }
        //14 Contact tracing report
        static void ContactTracingReporting(List<Person> personList, List<BusinessLocation> blist)
        {

            List<String> selist = new List<string>();
            bool bfound = false;


            double loop = 1;

            while (loop == 1)
            {
                Console.Write("Enter a Date for the Contract tracing report: ");
                try
                {
                    DateTime date = Convert.ToDateTime(Console.ReadLine());

                    Console.Write("Enter Business Name: ");
                    string tbizname = Console.ReadLine();

                    foreach (Person p in personList)
                    {

                        foreach (var se in p.SafeEntryList)
                        {
                            if (date <= se.CheckOut)
                            {
                                if (tbizname.ToUpper() == se.location.BusinessName.ToUpper())
                                {
                                    bfound = true;

                                    selist.Add(p.Name + "," + Convert.ToString(se.CheckIn) + "," + Convert.ToString(se.CheckOut) + "," + se.location.BusinessName);

                                }
                            }
                        }
                        loop = 0;

                    }

                    if (!bfound)
                    {
                        Console.WriteLine("Invalid Business name entered! Please try again!");
                    }


                }

                catch
                {
                    Console.WriteLine("Invalid Date entered! Please try again!");

                }

                if (loop == 0)
                {
                    using (StreamWriter sw = new StreamWriter("ContractTracing.csv", false))
                    {
                        sw.WriteLine("Name" + "," + "Check-in Time" + "," + "Check-out time" + "," + "Location");
                        foreach (string s in selist)
                        {
                            sw.WriteLine(s);
                        }
                        Console.WriteLine("Contract Tracing Reported Generated");
                        Console.WriteLine();
                    }
                }

            }


        }

        // 15 ADVANCED SHN STATUS REPORTING
        static void SHNStatusReporting(List<Person>personList)
        {
            List<String> temp = new List<String>();
            while(true)
            {
                Console.Write("Enter a date for Status Reporting: ");
                try
                {
                    DateTime date = Convert.ToDateTime(Console.ReadLine());
                    

                    foreach (Person p in personList)
                    {
                        foreach (TravelEntry te in p.TravelEntryList)
                        {
                            if (date <= te.ShnEndDate)
                            {
                                if (te.LastCountryOfEmbarkation != "")
                                {

                                    temp.Add(p.Name + "," + Convert.ToString(te.ShnEndDate) + "," + te.ShnStay.FacilityName);

                                }


                            }

                        }



                    }
                    break;
                }
                catch
                {
                    Console.WriteLine("Invalid Date Entered! Please try again.");
                }
                
            }
            
            using (StreamWriter sw = new StreamWriter("Status.csv", false))
            {
                sw.WriteLine("Name"+","+ "travelShnEndDate"+","+ "Facility Name");
                foreach (string s in temp)
                {
                    
                    
                    sw.WriteLine(s);
                    
                    
                    
                }
            }
            Console.WriteLine("Data successfully reported.");
            Console.WriteLine();

            
        }

        // 16 BONUS FEATURE
        static void CovidChecking()
        {
            double loop = 1;
            Console.WriteLine("This is a COIVD-19 Declaration form to enter/use a public facility.");
            Console.WriteLine("===================================================================");
            
            while(loop==1)
            {
                Console.Write("Have you in the past 14 days been in close contact with a confirmed COVID-19 Case? ");
                string ans = Console.ReadLine();
                if (ans.ToUpper() == "YES" || ans.ToUpper() == "Y")
                {
                    Console.WriteLine("Please go for a checkup or a Covid-19 swab test!");
                    Console.WriteLine("You are not allowed to enter/use this public facility");
                    loop = 0;
                }
                else if (ans.ToUpper() == "NO" || ans.ToUpper() == "N")
                {
                    loop = 0;
                    double troo = 1;
                    while(troo==1)
                    {
                        Console.Write("Have you been experiencing any symptoms such as fever (38 Degrees & above), Runny Nose, Cough, Sore Throat, Loss of Smell or Taste? ");
                        ans = Console.ReadLine();
                        if (ans.ToUpper() == "YES" || ans.ToUpper() == "Y")
                        {
                            troo = 0;
                            Console.WriteLine("Please go for a checkup or a Covid-19 swab test!");
                            Console.WriteLine("You are not allowed to enter/use this public facility");
                        }
                        else if (ans.ToUpper() == "NO" || ans.ToUpper() == "N")
                        {
                            troo = 0;
                            double tru = 1;
                            while (tru ==1)
                            {
                                Console.Write("Are you on medical leave and/or waiting for COVID-19 swab test results? ");
                                ans = Console.ReadLine();
                                if (ans.ToUpper() == "YES" || ans.ToUpper() == "Y")
                                {
                                    tru = 0;
                                    Console.WriteLine("You are not allowed to enter/use this public facility");
                                    Console.WriteLine("Please come back again once you are certain you have not contracted Covid-19");
                                }
                                else if (ans.ToUpper() == "NO" || ans.ToUpper() == "N")
                                {
                                    tru = 0;
                                    double trew = 1;
                                    while(trew==1)
                                    {
                                        Console.Write("Are you staying with an adult household member(>=18 years old) who is unwell with flu-like symptoms? ");
                                        ans = Console.ReadLine();
                                        if (ans.ToUpper() == "YES" || ans.ToUpper() == "Y")
                                        {
                                            trew = 0;
                                            Console.WriteLine("Please go for a checkup or a Covid-19 swab test!");
                                            Console.WriteLine("You are not allowed to enter/use this public facility");
                                        }
                                        else if (ans.ToUpper() == "NO" || ans.ToUpper() == "N")
                                        {
                                            trew = 0;
                                            double truee = 1;
                                            while(truee==1)
                                            {
                                                Console.Write("Are you staying with a household member serving a Home Quarantine Order or Stay-Home Notice at your place of residence? ");
                                                ans = Console.ReadLine();
                                                if (ans.ToUpper() == "YES" || ans.ToUpper() == "Y")
                                                {
                                                    truee = 0;
                                                    Console.WriteLine("You are not allowed to enter/use this public facility");
                                                    Console.WriteLine("Please come back again once their Quarantine order is over!");
                                                }
                                                else if (ans.ToUpper() == "NO" || ans.ToUpper() == "N")
                                                {
                                                    truee = 0;
                                                    double a = 1;
                                                    while (a == 1)
                                                    {
                                                        Console.Write("Are you working in an environment with risk of exposure to COVID-19 cases in the last 14 days e.g. dormitory,isolation/quarantine/community care facilities or ambulance/dedicated patient transport? ");
                                                        ans = Console.ReadLine();
                                                        if (ans.ToUpper() == "YES" || ans.ToUpper() == "Y")
                                                        {
                                                            a = 0;
                                                            Console.WriteLine("Please go for a checkup or a Covid-19 swab test!");
                                                            Console.WriteLine("You are not allowed to enter/use this public facility");
                                                            Console.WriteLine("Come back once you have received a favourable result in the swab test!");
                                                        }
                                                        else if (ans.ToUpper() == "NO" || ans.ToUpper() == "N")
                                                        {
                                                            a = 0;
                                                            double b = 1;
                                                            while(b==1)
                                                            {
                                                                Console.Write("Would you like to submit the form? ");
                                                                ans = Console.ReadLine();
                                                                if (ans.ToUpper() == "YES" || ans.ToUpper() == "Y")
                                                                {
                                                                    b = 0;
                                                                    Console.WriteLine("Done!");
                                                                }
                                                                else if (ans.ToUpper() == "NO" || ans.ToUpper() == "N")
                                                                {
                                                                    b = 0;
                                                                    double c = 1;
                                                                    while(c==1)
                                                                    {
                                                                        Console.Write("Would you like to resubmit the form? ");
                                                                        ans = Console.ReadLine();
                                                                        if (ans.ToUpper() == "YES" || ans.ToUpper() == "Y")
                                                                        {
                                                                            c = 0;
                                                                            CovidChecking();
                                                                        }
                                                                        else if (ans.ToUpper() == "NO" || ans.ToUpper() == "N")
                                                                        {
                                                                            c = 0;
                                                                            Console.WriteLine("Leaving...");
                                                                            Console.WriteLine();
                                                                        }
                                                                        else
                                                                        {
                                                                            Console.WriteLine("Please enter either 'Yes' or 'No' ");
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    Console.WriteLine("Please enter either 'Yes' or 'No' ");
                                                                }
                                                            }
                                                            
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Please enter either 'Yes' or 'No' ");
                                                        }
                                                    }
                                                    
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Please enter either 'Yes' or 'No' ");
                                                }
                                            }
                                            

                                        }
                                        else
                                        {
                                            Console.WriteLine("Please enter either 'Yes' or 'No' ");
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    Console.WriteLine("Please enter either 'Yes' or 'No' ");
                                }
                            }
                            
                        }
                        else
                        {
                            Console.WriteLine("Please enter either 'Yes' or 'No' ");
                        }
                    }
                    
                }
                else
                {
                    Console.WriteLine("Please enter either 'Yes' or 'No' ");
                }
            }
            
        }
    }
}

