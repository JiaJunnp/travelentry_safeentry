﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TEST_ASG2
{
    abstract class Person
    {

        //Attributes
        public string Name { get; set; }
        public List<SafeEntry> SafeEntryList { get; set; }
        public List<TravelEntry> TravelEntryList { get; set; }


        //Constructors
        public Person() { }
        public Person(string n)
        {
            Name = n;
            TravelEntryList = new List<TravelEntry>();
            SafeEntryList = new List<SafeEntry>();
        }

        //Methods
        public void AddTravelEntry(TravelEntry te)
        {
            TravelEntryList.Add(te);
        }
        public void AddSafeEntry(SafeEntry se)
        {
            SafeEntryList.Add(se);
        }

        public abstract double CalculateSHNCharges();

        public override string ToString()
        {
            return base.ToString();
        }
    }


    
    
}
