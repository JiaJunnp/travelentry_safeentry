﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Globalization;

namespace TEST_ASG2
{
    class Program
    {

        //Main method
        static void Main(string[] args)
        {
            //Create Person and Business Location List 
            List<Person> personList = new List<Person>();
            List<BusinessLocation> blist = new List<BusinessLocation>();
            List<SHNFacility> SHNFlist = new List<SHNFacility>();

            InitSHNFdata(SHNFlist);
            InitPersonList(personList);

            /* save for later use if necessary */
            /*foreach (Person p in personList)
            {
                if (p.Name.ToUpper()=="ALICE")
                {
                    foreach(TravelEntry te in p.TravelEntryList)
                    {
                        foreach(SHNFacility sf in SHNFlist)
                        {
                            if (te.LastCountryOfEmbarkation.ToUpper()=="AUSTRALIA")
                            {
                                if (sf.FacilityName.ToUpper() == "YOZEL")
                                {
                                    te.AssignSHNFacility(sf);
                                }
                            }
                            
                        }
                    }
                }
                else if(p.Name.ToUpper()=="JACKSON")
                {
                    foreach(TravelEntry te in p.TravelEntryList)
                    {
                        if (te.LastCountryOfEmbarkation.ToUpper()=="INDONESIA")
                        {
                            foreach(SHNFacility sf in SHNFlist)
                            {
                                if(sf.FacilityName.ToUpper()=="YOZEL")
                                {
                                    te.AssignSHNFacility(sf);
                                }
                            }
                        }
                    }
                }
                else if(p.Name.ToUpper()=="JAMES")
                {
                    foreach(TravelEntry te in p.TravelEntryList)
                    {
                        if(te.LastCountryOfEmbarkation.ToUpper()=="GERMANY")
                        {
                            foreach(SHNFacility sf in SHNFlist)
                            {
                                if(sf.FacilityName.ToUpper()=="MANDARIN ORCHID")
                                {
                                    te.AssignSHNFacility(sf);
                                }
                            }
                        }
                    }
                }
                else if (p.Name.ToUpper()=="EVELYN")
                {
                    foreach(TravelEntry te in p.TravelEntryList)
                    {
                        if(te.LastCountryOfEmbarkation.ToUpper()=="CANADA")
                        {
                            foreach(SHNFacility sf in SHNFlist)
                            {
                                if(sf.FacilityName.ToUpper()=="SMALL HOSTEL")
                                {
                                    te.AssignSHNFacility(sf);
                                }
                            }
                        }
                    }
                }
            }*/
            

            while (true)
            {
                DisplayMenu();
                Console.Write("Enter Option(or 0 to exit): ");
                string option = Console.ReadLine();
                if (option == "1")
                {
                    InitPersonList(personList);
                    InitBusinessList(blist);
                    Console.WriteLine("Person and Business Data Loaded.");
                    Console.WriteLine("");
                }
                else if (option == "2")
                {
                    InitSHNFdata(SHNFlist);
                    Console.WriteLine("SHN Facility Data Loaded.");
                    Console.WriteLine("");
                }
                else if (option == "3")
                {
                    ListVisitors(personList);
                }
                else if (option == "4")
                {
                    ListPersonDetails(personList);
                }
                else if (option == "5")
                {
                    AssignReplaceTTToken(personList);
                }
                else if (option == "6")
                {
                    ListBizLocations(blist);
                }
                else if (option == "7")
                {
                    EditBizLocations(blist);
                }
                else if (option == "8")
                {
                    SafeEntryCheckin(personList, blist);
                }
                else if (option == "9")
                {
                    SECheckout(personList);
                }
                else if (option == "10")
                {
                    DisplaySHNFacility(SHNFlist);

                }
                else if (option == "11")
                {
                    CreateVisitor(personList);
                }
                else if (option == "12")
                {
                    CreateTravelEntryRecord(personList, SHNFlist);
                }
                else if (option == "13")
                {
                    CalculateSHNCharges(personList, SHNFlist);
                }
                else if (option == "0")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Option! Please try again..");
                    Console.WriteLine();
                }

            }



        }


        //DisplayMenuMethod
        static void DisplayMenu()
        {
            Console.WriteLine("1) Load Person and Business Location Data");
            Console.WriteLine("2) Load SHN Facility Data");
            Console.WriteLine("3) List all Visitors");
            Console.WriteLine("4) List Person Details");
            Console.WriteLine("5) Assign/Replace TraceTogether Token");
            Console.WriteLine("6) List all Business Locations");
            Console.WriteLine("7) Edit Business Location Capacity");
            Console.WriteLine("8) SafeEntry Check-in");
            Console.WriteLine("9) SafeEntry Check-out");
            Console.WriteLine("10) List all SHN Facilities");
            Console.WriteLine("11) Create Visitor");
            Console.WriteLine("12) Create TravelEntry Record");
            Console.WriteLine("13) Calculate SHN Charges");

        }

        //****BASIC FEATURES****

        //===GENERAL FEATURES==
        // 1) Load Person and Business Location Data Methods
        static void InitPersonList(List<Person> personList)
        {
            

            using (StreamReader sr = new StreamReader("C:\\Users\\user\\source\\repos\\testestsetsetsetstset\\testestsetsetsetstset\\Person.csv"))
            {
                string s = sr.ReadLine();
                while ((s = sr.ReadLine()) != null)
                {
                    string[] items = s.Split(',');
                    if (items[0].ToUpper() == "RESIDENT")
                    {
                        DateTime expiryDate;
                        DateTime.TryParseExact(items[8], "dd/MMM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiryDate);
                        TraceTogetherToken token = new TraceTogetherToken(items[6], items[7], expiryDate);


                        Person r1 = new Resident(items[1], items[2], Convert.ToDateTime(items[3]), token);

                        if (items[9]!="")
                        {
                            TravelEntry te = new TravelEntry(items[9], items[10], Convert.ToDateTime(items[11]));

                            te.ShnEndDate = Convert.ToDateTime(items[12]);
                            if (items[14].ToUpper()!="")
                            {
                                te.ShnStay.FacilityName = items[14];
                                te.AssignSHNFacility(te.ShnStay);
                                
                                


                            }
                            else
                            {
                                te.ShnStay.FacilityName = null;
                            }
                            if (items[13].ToUpper() == "TRUE")
                            {
                                te.isPaid = true;
                            }
                            else if (items[13].ToUpper() == "FALSE")
                            {
                                te.isPaid = false;
                            }

                            r1.AddTravelEntry(te);


                        }                      
                        personList.Add(r1);

                    }
                    else if (items[0].ToUpper() == "VISITOR")
                    {
                        Person v1 = new Visitor(items[1], items[4], items[5]);
                        if (items[9] != "")
                        {
                            TravelEntry te = new TravelEntry(items[9], items[10], Convert.ToDateTime(items[11]));

                            te.ShnEndDate = Convert.ToDateTime(items[12]);

                            if (items[14].ToUpper() != "")
                            {
                                te.ShnStay.FacilityName = items[14].ToUpper();
                                te.AssignSHNFacility(te.ShnStay);
                                


                            }
                            else
                            {
                                te.ShnStay.FacilityName = null;
                            }
                            if (items[13].ToUpper() == "TRUE")
                            {
                                te.isPaid = true;
                            }
                            else if (items[13].ToUpper()=="FALSE")
                            {
                                te.isPaid = false;
                            }

                            v1.AddTravelEntry(te);
                        }
                        personList.Add(v1);
                    }

                }

            }

            Console.WriteLine("Data successfully initialised.");
        }




        static void InitBusinessList(List<BusinessLocation> blist)
        {
            string[] bdetails = File.ReadAllLines("BusinessLocation.csv");
            for (int i = 1; i < bdetails.Length; i++)
            {
                string[] bdata = bdetails[i].Split(",");
                string bname = bdata[0];
                string bcode = bdata[1];
                int maxcap = Convert.ToInt32(bdata[2]);
                BusinessLocation business = new BusinessLocation(bname, bcode, maxcap);
                blist.Add(business);
            }
        }







        // 2) Load SHN Facility Data  (API)
        static void InitSHNFdata(List<SHNFacility> SHNFlist)
        {
            List<SHNFacility> SHNFList = new List<SHNFacility>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    SHNFList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);


                    foreach (SHNFacility sf in SHNFList)
                    {
                        SHNFacility sf1 = new SHNFacility(sf.FacilityName, sf.FacilityCapacity, sf.DistFromAirCheckpoint, sf.DistFromSeaCheckpoint, sf.DistFromLandCheckpoint);
                        SHNFlist.Add(sf1);
                        
                        
                        

                    }


                }
            }
            Console.WriteLine("Data successfully initialised.");
        }



        // 3) List all Visitors
        static void ListVisitors(List<Person> personList)
        {
            Console.WriteLine("{0, -15} {1, -15} {2, -15}", "Name", "PassportNo", "Nationality");
            foreach (var i in personList)
            {
                if (i is Visitor)
                {
                    Visitor v = (Visitor)i;
                    Console.WriteLine("{0, -15} {1, -15} {2, -15}", v.Name, v.PassportNo, v.Nationality);
                }

            }
        }


        //===SafeEntry/TraceTogether===

        // 4) List Person Details
        static void ListPersonDetails(List<Person>personList)
        {
            Console.Write("Enter your name ");
            string name = Console.ReadLine();
            foreach (Person p in personList)
            {
                if (p.Name.ToUpper() == name.ToUpper())
                {

                    if (p is Visitor)
                    {
                        Visitor v = (Visitor)p;
                        foreach (TravelEntry te in v.TravelEntryList)
                        {
                            Console.WriteLine("{0,-25} {1,-10} {2,-21} {3,-21} {4,-7} {5}", "LastCountryOfEmbarkation", "EntryMode", "EntryDate", "ShnDate", "IsPaid", "Facility Name");
                            Console.WriteLine("{0,-25} {1,-10} {2,-21} {3,-21} {4,-7} {5}", te.LastCountryOfEmbarkation, te.EntryMode, te.EntryDate, te.ShnEndDate, te.isPaid, te.ShnStay.FacilityName);
                        }
                    }
                    else if (p is Resident)
                    {

                        Resident r = (Resident)p;


                    }

                }
            }
            


                

            

        }

        // 5) Assign/Replace TraceTogether Token

        static void AssignReplaceTTToken(List<Person> personList)
        {
            Console.Write("Enter the resident name: ");
            string targetname = Console.ReadLine();
            bool namefound = false;
            foreach (Person p in personList)
            {
                if (targetname == p.Name)
                {
                    namefound = true;
                    if (p is Resident)
                    {
                        Resident r = (Resident)p;

                        if (r.token == null)  // if the resident does not have a tracetogether token
                        {

                            Console.WriteLine("No tracetogether token found. Assigning one now");
                            Console.Write("Enter token Serial number: ");
                            string serialno = Console.ReadLine();
                            Console.Write("Enter Collection location: ");
                            string collectlocation = Console.ReadLine();
                            DateTime newexpiredate = DateTime.Today.AddMonths(6);

                            TraceTogetherToken newtoken = new TraceTogetherToken(serialno, collectlocation, newexpiredate);
                            r.token = newtoken;

                            Console.WriteLine("New token assigned");
                            Console.WriteLine();
                            break;

                        }

                        else if (r.token != null) //If resident has token but has already expiresd
                        {

                            if (r.token.IsEligibleForReplacement())
                            {
                                Console.WriteLine("Token has expired. Replacing the token now");
                                Console.Write("Enter token Serial number: ");
                                string serialno = Console.ReadLine();
                                Console.Write("Enter Collection location: ");
                                string collectlocation = Console.ReadLine();
                                DateTime newexpiredate = DateTime.Today.AddMonths(6);


                                TraceTogetherToken newtoken = new TraceTogetherToken(serialno, collectlocation, newexpiredate);

                                r.token = newtoken;
                                Console.WriteLine("Token has been replaced");
                                Console.WriteLine();
                                break;
                            }

                            else if (!r.token.IsEligibleForReplacement())
                            {
                                Console.WriteLine("Token cannot be replaced");
                            }

                            else //resident has token and have no problems
                            {
                                Console.WriteLine("Resident has a token and have no problems");
                                Console.WriteLine();
                                break;
                            }
                        }


                    }
                }
                else if (!namefound)
                {
                    Console.WriteLine();
                    Console.WriteLine("Invalid name entered! ");
                }
            }


        }

        // 6) List all Business Locations (DONE)
        static void ListBizLocations(List<BusinessLocation> blist)
        {
            Console.WriteLine("{0,-20} {1,10} {2,20}", "Business Name", "Branch Code", "Maximum Capacity");
            foreach (var b in blist)
            {
                Console.WriteLine("{0,-20} {1,10} {2,20}", b.BusinessName, b.BranchCode, b.MaximumCapacity);
            }
        }



       
        // 7) Edit Business Location Capacity (DONE)
        static void EditBizLocations(List<BusinessLocation> blist)
        {
            Console.Write("Enter Business Name: ");
            string targetbizname = Console.ReadLine();

            bool found = false;
            foreach (var b in blist)
            {

                if (targetbizname.ToUpper() == b.BusinessName.ToUpper())
                {
                    found = true;
                    Console.Write("Enter new maximum capacity: ");
                    int newcap = Convert.ToInt32(Console.ReadLine());
                    b.MaximumCapacity = newcap;
                    Console.WriteLine("Maximum Capacity of {0} has been updated", targetbizname);
                }

            }
            if (!found)
            {
                Console.WriteLine("Invalid Name Entered! Please try again");
                EditBizLocations(blist);
            }


        }


        // 8) SafeEntry Check-in to be fixed (check if allready checked in)
        static void SafeEntryCheckin(List<Person> personlist, List<BusinessLocation> blist)
        {
            Console.WriteLine();
            Console.WriteLine("===Safe Entry CheckIn===");
            Console.Write("Enter your name: "); //prompt user
            string name = Console.ReadLine(); //get user name


            bool namefound = false;
            bool biznamefound = false;
            foreach (var p in personlist)
            {

                if (name.ToUpper() == p.Name.ToUpper())
                {
                    namefound = true;
                    ListBizLocations(blist);//list location
                    Console.Write("Enter the name of business location you are checking in: "); // prompt business location
                    string tbizname = Console.ReadLine(); // get name of location


                    foreach (var b in blist)  // check entire list
                    {
                        if (tbizname.ToUpper() == b.BusinessName.ToUpper()) // check if target business name exists
                        {
                            biznamefound = true;
                            if (b.IsFull())   //If location is full
                            {
                                Console.WriteLine("{0} is full!", tbizname);
                                Console.WriteLine();
                            }

                            else if (!b.IsFull()) //if location is not full 
                            {
                                DateTime Checkin = DateTime.Now;

                                b.VisitorsNow += 1;
                                BusinessLocation location = new BusinessLocation(b.BusinessName, b.BranchCode, b.MaximumCapacity);
                                SafeEntry se = new SafeEntry(Checkin, location);


                                p.AddSafeEntry(se);// add the safeentry object to p
                                Console.WriteLine("Checked in");
                                Console.WriteLine();

                            }

                        }

                    }
                    if (!biznamefound)
                    {

                        Console.WriteLine("Invalid Business Name Entered! ");
                        Console.WriteLine();
                    }


                }
            }
            if (!namefound)
            {

                Console.WriteLine("Invalid Name Entered! ");
                Console.WriteLine();
            }



        }



        // 9) SafeEntry Check-out havent do
        static void SECheckout(List<Person> personlist)
        {
            Console.WriteLine();
            Console.WriteLine("===Safe Entry CheckOut===");
            Console.Write("Enter your name: "); //prompt user
            string name = Console.ReadLine(); //get user name

            foreach (var p in personlist)
            {
                if (name.ToUpper() == p.Name.ToUpper())
                {
                    //code to be written here : check list if even in somewhere
                    Console.WriteLine("Safe entry records that have not been checked out ");

                }
            }
        }

        //===TravelEntry===
        // 10) List all SHN Facilities
        static void DisplaySHNFacility(List<SHNFacility> SHNFlist)
        {
            Console.WriteLine("{0,-16} {1} {2} {3} {4}", "Facility Name", "Capacity", "DistFromAirCheckpoint", "DistFromSeaCheckpoint", "DistFromLandCheckpoint");
            foreach (SHNFacility sf in SHNFlist)
            {
                Console.WriteLine("{0,-16} {1,-8} {2,-21} {3,-21} {4}", sf.FacilityName, sf.FacilityCapacity, sf.DistFromAirCheckpoint, sf.DistFromSeaCheckpoint, sf.DistFromLandCheckpoint);

            }
        }

        // 11) Create Visitor
        static void CreateVisitor(List<Person> personList)
        {
            Console.Write("Please enter your Name: ");
            string name = Console.ReadLine();
            Console.Write("Please enter your Passport Number: ");
            string no = Console.ReadLine();
            Console.Write("Please enter your Nationality: ");
            string nationality = Console.ReadLine();
            Person v1 = new Visitor(name, no, nationality);
            personList.Add(v1);
            Console.WriteLine(v1.Name + "'s profile successfully created!");
        }

        // 12) Create TravelEntry Record
        static void CreateTravelEntryRecord(List<Person> personList, List<SHNFacility> SHNFlist)
        {
            Console.Write("Please enter your Name: ");
            string name = Console.ReadLine();
            foreach (Person p in personList)
            {
                if (p.Name.ToUpper() == name.ToUpper())
                {
                    foreach(TravelEntry TE in p.TravelEntryList)
                    {
                        if(TE.ShnStay is null)
                        {
                            Console.Write("Please enter your Last Country Of Embarkation: ");
                            string country = Console.ReadLine();
                            Console.Write("Please enter your Entry Mode: ");
                            string entrymode = Console.ReadLine();
                            Console.Write("Please enter your Entry Date(YY/MM/DD): ");
                            DateTime entrydate = Convert.ToDateTime(Console.ReadLine()); // TO ADD TRY, EXCEPT

                            TravelEntry te = new TravelEntry(country, entrymode, entrydate);
                            te.CalculateSHNDuration();
                            DisplaySHNFacility(SHNFlist);
                            double loop = 0;
                    
                            
                            if (te.ShnStay.FacilityName is null)
                            {
                                if (te.ShnEndDate>te.EntryDate.AddDays(7))
                                {
                                    while (loop==0)
                                    {
                                        Console.Write("Please enter your chosen SHN Facility: ");
                                        string facility = Console.ReadLine();

                                        foreach (SHNFacility shnf in SHNFlist)
                                        {
                                            if (shnf.FacilityName.ToUpper() == facility.ToUpper())
                                            {
                                                                                                                                                                                                                   
                                                if (shnf.IsAvailable())
                                                {
                                            
                                                    te.AssignSHNFacility(shnf);
                                                    Console.WriteLine("You have been assigned to Facility " + facility);
                                            
                                                    loop = 1;
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine(shnf.FacilityName + " is currently fully occupied. Please chooose another SHN Facility");
                                                }

                                            }

                                        }
                                    }
                                }
                                else
                                {

                                    Console.WriteLine("No SHN Facility allocation required.");
                                }
                                p.AddTravelEntry(te); 






                            }  
                        }
                        else if (TE.ShnStay != null)
                        {
                            Console.WriteLine("You already have a Travel Entry record!");
                            break;
                        }
                    }
                                                                                        

                }
            }
        }

        // 13) Calculate SHN Charges
        static void CalculateSHNCharges(List<Person> personList,List<SHNFacility>SHNFlist)
        {
            Console.Write("Please enter your Name: ");
            string name = Console.ReadLine();
            int count = -1;
            foreach(Person p in personList)
            {

                count += 1;
                if (p.Name.ToUpper() == name.ToUpper())
                {
                                                    
                    
                    if(p is Visitor)
                    {

                        Visitor v = (Visitor)p;

                        

                                            
                                            
                        foreach (TravelEntry te in p.TravelEntryList)
                        {
                            if (te.EntryDate <= DateTime.Now)
                            {
                                if (te.isPaid is false)
                                {
                                    if (te.LastCountryOfEmbarkation.ToUpper() == "NEW ZEALAND" || te.LastCountryOfEmbarkation.ToUpper() == "VIETNAM" || te.LastCountryOfEmbarkation.ToUpper() == "MACAO SAR")
                                    {

                                        
                                        double shnc= v.CalculateSHNCharges();
                                        double travel = (80 + (80 * 0.07));
                                        double cost = shnc + travel;
                                        while (true)
                                        {
                                            Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                            Console.Write("Would you like to make payment?(Y/N) ");
                                            string option = Console.ReadLine();
                                            if (option.ToUpper() == "Y")
                                            {
                                                Console.WriteLine("Payment Done!");
                                                te.isPaid = true;
                                            }
                                            else if (option.ToUpper() == "N")
                                            {
                                                Console.WriteLine("Payment shall be made at another date!");
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        double shnc = v.CalculateSHNCharges();
                                        if (te.ShnStay != null)
                                        {
                                            double travel=(te.ShnStay.CalculateTravelCost(te.EntryMode,te.EntryDate));
                                            double gst = travel * 0.07;
                                            double cost = travel + shnc +gst;
                                            while (true)
                                            {
                                                Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                                Console.Write("Would you like to make payment?(Y/N) ");
                                                string option = Console.ReadLine();
                                                if (option.ToUpper() == "Y")
                                                {
                                                    Console.WriteLine("Payment Done!");
                                                    te.isPaid = true;
                                                }
                                                else if (option.ToUpper() == "N")
                                                {
                                                    Console.WriteLine("Payment shall be made at another date!");
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                                }
                                            }
                                        }
                                        
                                        

                                    }
                                }
                                
                            }
                                               

                        }
                                            
                        // END of new stuff // new stuff // new stuff // new stuff //new stuff // new stuff

                        
                     
                    }
                    else if (p is Resident)
                    {
                        Resident r = (Resident)p;
                        foreach (TravelEntry te in p.TravelEntryList)
                        {
                            if (te.EntryDate <= DateTime.Now)
                            {
                                if (te.isPaid is false)
                                {
                                    if (te.LastCountryOfEmbarkation.ToUpper() == "NEW ZEALAND" || te.LastCountryOfEmbarkation.ToUpper() == "VIETNAM")
                                    {


                                        double cost = r.CalculateSHNCharges();
                                        while (true)
                                        {
                                            Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                            Console.Write("Would you like to make payment?(Y/N) ");
                                            string option = Console.ReadLine();
                                            if (option.ToUpper() == "Y")
                                            {
                                                Console.WriteLine("Payment Done!");
                                                te.isPaid = true;
                                            }
                                            else if (option.ToUpper() == "N")
                                            {
                                                Console.WriteLine("Payment shall be made at another date!");
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                            }
                                        }

                                    }
                                    else if (te.LastCountryOfEmbarkation.ToUpper()=="MACAO SAR")
                                    {
                                        double shnc = r.CalculateSHNCharges();
                                        double travel = 20 + (20 * 0.07);
                                        double cost = travel + shnc;
                                        while (true)
                                        {
                                            Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                            Console.Write("Would you like to make payment?(Y/N) ");
                                            string option = Console.ReadLine();
                                            if (option.ToUpper() == "Y")
                                            {
                                                Console.WriteLine("Payment Done!");
                                                te.isPaid = true;
                                            }
                                            else if (option.ToUpper() == "N")
                                            {
                                                Console.WriteLine("Payment shall be made at another date!");
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        double shnc = r.CalculateSHNCharges();
                                        if (te.ShnStay != null)
                                        {
                                            double travelSDF = 1020 + (1020 * 0.07);
                                            double cost = travelSDF + shnc;
                                            while (true)
                                            {
                                                Console.WriteLine("Your total is " + cost.ToString("$0.00"));
                                                Console.Write("Would you like to make payment?(Y/N) ");
                                                string option = Console.ReadLine();
                                                if (option.ToUpper() == "Y")
                                                {
                                                    Console.WriteLine("Payment Done!");
                                                    te.isPaid = true;
                                                }
                                                else if (option.ToUpper() == "N")
                                                {
                                                    Console.WriteLine("Payment shall be made at another date!");
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Invalid Option! Please Enter either 'Y' or 'N'");
                                                }
                                            }
                                        }



                                    }
                                }

                            }


                        }
                    }
                    
                                

                            

                        
                    
                }
            }
            

        }
    }
}